/* This file is part of InstallOffline
 *
 *   InstallOffline
 *   Copyright (C) 2014  Slamet Badwi
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDir>
#include <QStringList>
#include <QDebug>
#include <QFile>
#include <QByteArray>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    dlDialog = new DLDialog(this);
    info = new InfoDialog(this);

#if (QT_VERSION < QT_VERSION_CHECK(5, 0, 0))
    QJson::Parser parser;
    bool ok;
#endif

    groupVer = new QButtonGroup(this);
    groupVer->addButton(ui->choosePrecise);
    groupVer->setId(ui->choosePrecise,1);
    groupVer->addButton(ui->chooseTrusty);
    groupVer->setId(ui->chooseTrusty,2);
    groupVer->addButton(ui->chooseUtopic);
    groupVer->setId(ui->chooseUtopic,3);

    groupLng = new QButtonGroup(this);
    groupLng->addButton(ui->chooseEng);
    groupLng->setId(ui->chooseEng,1);
    groupLng->addButton(ui->chooseInd);
    groupLng->setId(ui->chooseInd,2);

    groupArc = new QButtonGroup(this);
    groupArc->addButton(ui->chooseX86);
    groupArc->setId(ui->chooseX86,1);
    groupArc->addButton(ui->chooseX64);
    groupArc->setId(ui->chooseX64,2);

    // mengisi combo box repositori
    QFile lumbung("lumbung.json");
    if(lumbung.exists())
    {
        lumbung.open(QIODevice::ReadOnly|QIODevice::Text);
        QByteArray repoList = lumbung.readAll();

#if (QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
        QJsonParseError err;
        QJsonDocument doc = QJsonDocument::fromJson(repoList,&err);
        QJsonObject obj = doc.object();
        if(!obj.value("lumbung").isNull())
        {
            QJsonValue repo = obj.value("lumbung");
            QJsonArray list = repo.toArray();

            for(int i = 0;i<list.size();++i)
            {
                QJsonObject name = list.at(i).toObject();
                ui->chooseRepo->insertItem(i,name.value("nama").toString());
                ui->chooseRepo->setItemData(i,name.value("alamat").toString());
            }
            //qDebug() << list;
        }

#else
        QVariantMap hasil = parser.parse(repoList,&ok).toMap();
        int i = 0;
        foreach (QVariant repo, hasil["lumbung"].toList())
        {
            //qDebug() << repo;
            QVariantMap repoLists = repo.toMap();
            //qDebug() << repoLists["alamat"].toString();
            ui->chooseRepo->insertItem(i,repoLists["nama"].toString());
            ui->chooseRepo->setItemData(i,repoLists["alamat"].toString());
            i++;
            //qDebug() << i;
        }
#endif

        lumbung.close();
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionQuit_triggered()
{
    qApp->quit();
}

void MainWindow::on_actionAbout_triggered()
{
    info->exec();
}

void MainWindow::on_actionAbout_Qt_triggered()
{
    qApp->aboutQt();
}

void MainWindow::on_getLinks_clicked()
{
    //dlDialog->show();
    qDebug() << groupVer->checkedId();
    qDebug() << groupLng->checkedId();
    qDebug() << groupArc->checkedId();
    qDebug() << ui->chooseRepo->itemData(ui->chooseRepo->currentIndex()).toString();
}

void MainWindow::on_chooseRepo_currentIndexChanged(int index)
{
    ui->statusBar->showMessage(ui->chooseRepo->itemData(index).toString());
    //qDebug() << ui->chooseRepo->currentIndex();
}
