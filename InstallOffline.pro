#-------------------------------------------------
#
# Project created by QtCreator 2014-04-12T19:49:12
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = InstallOffline
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    dldialog.cpp \
    infodialog.cpp

HEADERS  += mainwindow.h \
    dldialog.h \
    infodialog.h

FORMS    += mainwindow.ui \
    dldialog.ui \
    infodialog.ui
TRANSLATIONS += io_en.ts \
                io_id.ts
equals(QT_MAJOR_VERSION, 4) {
    LIBS += -L/usr/local/lib -lqjson
}
