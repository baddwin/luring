/* This file is part of InstallOffline
 *
 *   InstallOffline
 *   Copyright (C) 2014  Slamet Badwi
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "dldialog.h"
#include "infodialog.h"
#include <QMainWindow>
#include <QStringList>
#include <QButtonGroup>

#if(QT_VERSION >= QT_VERSION_CHECK(5, 0, 0))
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#else   // install libqjson0 & libqjson-dev
#include <QtDeclarative/QtDeclarative>
#include <qjson/parser.h>
#include <QVariantMap>
#endif

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionQuit_triggered();

    void on_actionAbout_triggered();

    void on_getLinks_clicked();

    void on_actionAbout_Qt_triggered();

    void on_chooseRepo_currentIndexChanged(int index);

private:
    Ui::MainWindow *ui;
    QButtonGroup *groupVer;
    QButtonGroup *groupLng;
    QButtonGroup *groupArc;
    DLDialog *dlDialog;
    InfoDialog *info;
};

#endif // MAINWINDOW_H
